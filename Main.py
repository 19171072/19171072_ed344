import connection as c
import tkinter as tk
from tkinter import *
window = tk.Tk()
c.LeonardoConnection.connect(c.LeonardoConnection, "COM3")
data = 0;
def print_treble(val):
    print(val)
    c.LeonardoConnection.write(0)
    c.LeonardoConnection.write(0)
    c.LeonardoConnection.write(0)
    c.LeonardoConnection.write(0)
    c.LeonardoConnection.write(115)
    c.LeonardoConnection.write(11)
    c.LeonardoConnection.write(253)
def print_bass(val):
    print(val)
window.geometry("200x300")
window.wm_title("Mixer")
label = tk.Label(window, text="Bass").pack(side=RIGHT)
treble = tk.Scale(window, to=0, from_=100, length=300, width=30, command=print_treble)
treble.pack(side=LEFT)
treble.set(50)
bass = tk.Scale(window, from_=100, to=0, length=300, width=30, command=print_bass)
bass.pack(side=RIGHT)
bass.set(50)
label = tk.Label(window, text="Treble").pack(side=LEFT)
window.mainloop()


